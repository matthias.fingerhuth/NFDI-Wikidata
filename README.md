# NFDI-Wikidata

This repository is about analyses and visualizations of NFDI drawing on Wikidata.

## NFDI_Wikidata_map_by_subject_areas

A map of participation in NFDI consortia. Subject areas are assigned in Wikidata based on the classification used at the NFDI-Conferences.

## NFDI-Wikidata Plots.ipynb

A very basic plot of institutional participation in NFDI consortia.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/mfingerh/NFDI-Wikidata/HEAD?labpath=NFDI-Wikidata%20Plots.ipynb) for NFDI-Wikidata Plots
